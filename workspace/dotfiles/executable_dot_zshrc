#!/bin/zsh

# For debugging. This must be the very first line in the rc file. Don't forget to uncomment the zshprof line at the bottom too.
# zmodload zsh/zprof

# Caching for better startup performance
autoload -Uz compinit
compinit -C

# increase allowed amount of files open. Needed for nvim.
ulimit -n 10240

# Install zinit if not already installed
if ! command -v zinit &> /dev/null
then
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/zdharma-continuum/zinit/main/doc/install.sh)"
fi

# Setup for zinit plugin manager
ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"
[ ! -d $ZINIT_HOME ] && mkdir -p "$(dirname $ZINIT_HOME)"
[ ! -d $ZINIT_HOME/.git ] && git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
source "${ZINIT_HOME}/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# Plugins
zinit ice wait lucid atinit"zicompinit; zicdreplay" \
    zdharma-continuum/fast-syntax-highlighting
zinit ice wait lucid atload"_zsh_autosuggest_start" \
    zsh-users/zsh-autosuggestions
zinit ice blockf atpull'zinit creinstall -q .' \
    zsh-users/zsh-completions
zinit ice wait lucid \
    agkozak/zsh-z
zinit ice wait lucid \
    junegunn/fzf

source ./aliases
source ./prompt-theme

# General other settings
EDITOR=nvim

# For debugging. This must be at the very end of the file, and don't forget to uncomment the zmodload line at the top
# zprof